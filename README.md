# Khrypt
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)

Khrypt is a library project that aims to implement many popular as well as less known a cryptographic functions and protocols.

## Why am I making another such library?
Now when you know more or less what is this project, I can explain why I'm doing it.
So, the library is being written mainly for self-education of the author on issues related to cryptography. However, that doesn't mean the library will never be used, on the contrary, I have some ideas and I count on it will gain its own community.

# License
This project is released under the MIT license - see the [LICENSE](LICENSE) file for details.
